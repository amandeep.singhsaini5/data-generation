package com.esq.cashmanagement.data_generation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.esq.cashmanagement.data_generation.entity.Atd;

/**
 * @author Amandeep
 */
@Repository
public interface AtdRepository extends JpaRepository<Atd,Long> {
	List<Atd> findByAtmKeyIn(List<Long> atmkeys);
}
