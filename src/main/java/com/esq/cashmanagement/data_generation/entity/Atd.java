package com.esq.cashmanagement.data_generation.entity;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Amandeep
 */

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "ATD")
public class Atd {

    @Id
    @Column(name = "atmkey", nullable = false)
    private Long atmKey;

    @Column(name = "atdtimestamp", nullable = false)
    private Long atdTimestamp;

    @Column(name = "atdtime", nullable = false)
    private LocalDateTime atdTime;

    @Column(name = "recordtime", nullable = false)
    private LocalDateTime recordTime;

    @Column(name = "lasttranstimestamp", nullable = false)
    private Long lastTransTimestamp;

    @Column(name = "lasttranstime", nullable = false)
    private LocalDateTime lastTransTime;

    @Column(name = "laststatustimestamp", nullable = false)
    private Long lastStatusTimestamp;

    @Column(name = "laststatustime", nullable = false)
    private LocalDateTime lastStatusTime;

    @Column(name = "status", nullable = false)
    private Long status;

    @Column(name = "state", nullable = false)
    private Long state;

    @Column(name = "numfaults", nullable = false)
    private Long numFaults;

    @Column(name = "numdeposits", nullable = false)
    private Long numDeposits;

    @Column(name = "amtdeposits", nullable = false)
    private Long amtDeposits;

    @Column(name = "numpayments")
    private Long numPayments;

    @Column(name = "amtpayments", nullable = false)
    private Long amtPayments;

    @Column(name = "numchecks")
    private Long numChecks;

    @Column(name = "amtchecks", nullable = false)
    private Long amtChecks;

    @Column(name = "onusdebit", nullable = false)
    private Long onUsDebit;

    @Column(name = "onuscredit", nullable = false)
    private Long onUsCredit;

    @Column(name = "lastsettlementtime")
    private Date lastSettlementTime;

    @Column(name = "postingdate")
    private String postingDate;

    @Column(name = "userfld1")
    private String userFld1;

    @Column(name = "userfld2")
    private String userFld2;

    @Column(name = "hopper_1_contents")
    private String hopper_1_Contents;

    @Column(name = "hopper_1_billval")
    private Long hopper_1_billVal;

    @Column(name = "hopper_1_endcash")
    private Long hopper_1_EndCash;

    @Column(name = "hopper_1_cashout")
    private Long hopper_1_CashOut;

    @Column(name = "hopper_1_currencycode")
    private String hopper_1_currencyCode;

    @Column(name = "hopper_2_contents")
    private String hopper_2_Contents;

    @Column(name = "hopper_2_billval")
    private Long hopper_2_billVal;

    @Column(name = "hopper_2_endcash")
    private Long hopper_2_EndCash;

    @Column(name = "hopper_2_cashout")
    private Long hopper_2_CashOut;

    @Column(name = "hopper_2_currencycode")
    private String hopper_2_currencyCode;

    @Column(name = "hopper_3_contents")
    private String hopper_3_Contents;

    @Column(name = "hopper_3_billval")
    private Long hopper_3_billVal;

    @Column(name = "hopper_3_endcash")
    private Long hopper_3_EndCash;

    @Column(name = "hopper_3_cashout")
    private Long hopper_3_CashOut;

    @Column(name = "hopper_3_currencycode")
    private String hopper_3_currencyCode;

    @Column(name = "hopper_4_contents")
    private String hopper_4_Contents;

    @Column(name = "hopper_4_billval")
    private Long hopper_4_billVal;

    @Column(name = "hopper_4_endcash")
    private Long hopper_4_EndCash;

    @Column(name = "hopper_4_cashout")
    private Long hopper_4_CashOut;

    @Column(name = "hopper_4_currencycode")
    private String hopper_4_currencyCode;

    @Column(name = "hopper_5_contents")
    private String hopper_5_Contents;

    @Column(name = "hopper_5_billval")
    private Long hopper_5_billVal;

    @Column(name = "hopper_5_endcash")
    private Long hopper_5_EndCash;

    @Column(name = "hopper_5_cashout")
    private Long hopper_5_CashOut;

    @Column(name = "hopper_5_currencycode")
    private String hopper_5_currencyCode;

    @Column(name = "hopper_6_contents")
    private String hopper_6_Contents;

    @Column(name = "hopper_6_billval")
    private Long hopper_6_billVal;

    @Column(name = "hopper_6_endcash")
    private Long hopper_6_EndCash;

    @Column(name = "hopper_6_cashout")
    private Long hopper_6_CashOut;

    @Column(name = "hopper_6_currencycode")
    private String hopper_6_currencyCode;

    @Column(name = "totalcashout_defaultcurrency")
    private Long totalCashOut_Defaultcurrency;

    @Column(name = "totalendcash_defaultcurrency")
    private Long totalEndCash_Defaultcurrency;

    @Column(name = "totalcashout_othercurrency1")
    private Long totalCashOut_Othercurrency1;

    @Column(name = "totalendcash_othercurrency1")
    private Long totalEndCash_Othercurrency1;

    @Column(name = "totalcashout_othercurrency2")
    private Long totalCashOut_Othercurrency2;

    @Column(name = "totalendcash_othercurrency2")
    private Long totalEndCash_Othercurrency2;

    @Column(name = "totalcashout_othercurrency3")
    private Long totalCashOut_Othercurrency3;

    @Column(name = "totalendcash_othercurrency3")
    private Long totalEndCash_Othercurrency3;

    @Column(name = "lasttlftranstime")
    private Date lastTLFTransTime;

    /*@Column(name = "recordtimestamp", nullable = false)
    private String recordTimestamp;*/

    @Column(name = "hopper_1_stdbegcash")
    private Long hopper_1_StdbegCash;

    @Column(name = "hopper_1_begcash")
    private Long hopper_1_begCash;

    @Column(name = "hopper_1_cashincr")
    private Long hopper_1_CashIncr;

    @Column(name = "hopper_1_cashdecr")
    private Long hopper_1_CashDecr;

    @Column(name = "hopper_2_stdbegcash")
    private Long hopper_2_StdbegCash;

    @Column(name = "hopper_2_begcash")
    private Long hopper_2_begCash;

    @Column(name = "hopper_2_cashincr")
    private Long hopper_2_CashIncr;

    @Column(name = "hopper_2_cashdecr")
    private Long hopper_2_CashDecr;

    @Column(name = "hopper_3_stdbegcash")
    private Long hopper_3_StdbegCash;

    @Column(name = "hopper_3_begcash")
    private Long hopper_3_begCash;

    @Column(name = "hopper_3_cashincr")
    private Long hopper_3_CashIncr;

    @Column(name = "hopper_3_cashdecr")
    private Long hopper_3_CashDecr;

    @Column(name = "hopper_4_stdbegcash")
    private Long hopper_4_StdbegCash;

    @Column(name = "hopper_4_begcash")
    private Long hopper_4_begCash;

    @Column(name = "hopper_4_cashincr")
    private Long hopper_4_CashIncr;

    @Column(name = "hopper_4_cashdecr")
    private Long hopper_4_CashDecr;

    @Column(name = "hopper_5_stdbegcash")
    private Long hopper_5_StdbegCash;

    @Column(name = "hopper_5_begcash")
    private Long hopper_5_begCash;

    @Column(name = "hopper_5_cashincr")
    private Long hopper_5_CashIncr;

    @Column(name = "hopper_5_cashdecr")
    private Long hopper_5_CashDecr;

    @Column(name = "hopper_6_stdbegcash")
    private Long hopper_6_StdbegCash;

    @Column(name = "hopper_6_begcash")
    private Long hopper_6_begCash;

    @Column(name = "hopper_6_cashincr")
    private Long hopper_6_CashIncr;

    @Column(name = "hopper_6_cashdecr")
    private Long hopper_6_CashDecr;

    @Column(name = "numcmrcldeposits")
    private Long numCmrclDeposits;

    @Column(name = "amtcmrcldeposits")
    private Long amtCmrclDeposits;

    @Column(name = "cardsretained")
    private Long cardsRetained;

    @Column(name = "termdebit")
    private Long termDebit;

    @Column(name = "termcredit")
    private Long termCredit;

    @Column(name = "hopper_7_contents")
    private String hopper7Contents;

    @Column(name = "hopper_7_currencycode")
    private String hopper7currencyCode;

    @Column(name = "hopper_7_billval")
    private Long hopper7billVal;

    @Column(name = "hopper_7_stdbegcash")
    private Long hopper7StdbegCash;

    @Column(name = "hopper_7_begcash")
    private Long hopper7begCash;

    @Column(name = "hopper_7_endcash")
    private Long hopper7EndCash;

    @Column(name = "hopper_7_cashout")
    private Long hopper7CashOut;

    @Column(name = "hopper_7_cashincr")
    private Long hopper7CashIncr;

    @Column(name = "hopper_7_cashdecr")
    private Long hopper7CashDecr;

    @Column(name = "hopper_8_contents")
    private String hopper8Contents;

    @Column(name = "hopper_8_currencycode")
    private String hopper8currencyCode;

    @Column(name = "hopper_8_billval")
    private Long hopper8billVal;

    @Column(name = "hopper_8_stdbegcash")
    private Long hopper8StdbegCash;

    @Column(name = "hopper_8_begcash")
    private Long hopper8begCash;

    @Column(name = "hopper_8_endcash")
    private Long hopper8EndCash;

    @Column(name = "hopper_8_cashout")
    private Long hopper8CashOut;

    @Column(name = "hopper_8_cashincr")
    private Long hopper8CashIncr;

    @Column(name = "hopper_8_cashdecr")
    private Long hopper8CashDecr;

    @Column(name = "dualsiteind")
    private String dualSiteInd;

    @Column(name = "activenode")
    private String activeNode;

    @Column(name = "hopper_1_cashpurged")
    private Long hopper1CashPurged;

    @Column(name = "hopper_1_cashrejected")
    private Long hopper1CashRejected;

    @Column(name = "hopper_2_cashpurged")
    private Long hopper2CashPurged;
    
    @Column(name = "hopper_2_cashrejected")
    private Long hopper2CashRejected;

    @Column(name = "hopper_3_cashpurged")
    private Long hopper3CashPurged;

    @Column(name = "hopper_3_cashrejected")
    private Long hopper3CashRejected;

    @Column(name = "hopper_4_cashpurged")
    private Long hopper4CashPurged;

    @Column(name = "hopper_4_cashrejected")
    private Long hopper4CashRejected;

    @Column(name = "hopper_5_cashpurged")
    private Long hopper5CashPurged;

    @Column(name = "hopper_5_cashrejected")
    private Long hopper5CashRejected;

    @Column(name = "hopper_6_cashpurged")
    private Long hopper6CashPurged;

    @Column(name = "hopper_6_cashrejected")
    private Long hopper6CashRejected;

    @Column(name = "hopper_7_cashpurged")
    private Long hopper7CashPurged;

    @Column(name = "hopper_8_cashpurged")
    private Long hopper8CashPurged;

    @Column(name = "hopper_8_cashrejected")
    private Long hopper8cashrejected;

    @Column(name = "totalcashpurged")
    private Long totalCashPurged;

    @Column(name = "totalcashrejected")
    private Long totalCashRejected;

    @Column(name = "totalbegcash_defaultcurrency")
    private Long totalbegCashDefaultcurrency;

    @Column(name = "totalbegcash_othercurrency1")
    private Long totalbegCashOthercurrency1;

    @Column(name = "totalbegcash_othercurrency2")
    private Long totalbegCashOthercurrency2;

    @Column(name = "totalbegcash_othercurrency3")
    private Long totalbegCashOthercurrency3;

    @Column(name = "totaldispensedcurrbusinessday")
    private Long totalDispensedcurrbusinessDay;

    @Column(name = "lasttransactionamount")
    private Long lastTransactionAmount;

    @Column(name = "hopper_1d_billval")
    private Long hopper1DbillVal;

    @Column(name = "hopper_2d_billval")
    private Long hopper2DbillVal;

    @Column(name = "hopper_3d_billval")
    private Long hopper3DbillVal;

    @Column(name = "hopper_4d_billval")
    private Long hopper4DbillVal;

    @Column(name = "hopper_5d_billval")
    private Long hopper5DbillVal;

    @Column(name = "hopper_6d_billval")
    private Long hopper6DbillVal;

    @Column(name = "hopper_7d_billval")
    private Long hopper7DbillVal;

    @Column(name = "hopper_8d_billval")
    private Long hopper8DbillVal;

    @Column(name = "hopper_1d_currencycode")
    private Long hopper1DcurrencyCode;

    @Column(name = "hopper_2d_currencycode")
    private Long hopper2DcurrencyCode;

    @Column(name = "hopper_3d_currencycode")
    private Long hopper3DcurrencyCode;

    @Column(name = "hopper_4d_currencycode")
    private Long hopper4DcurrencyCode;

    @Column(name = "hopper_5d_currencycode")
    private Long hopper5DcurrencyCode;

    @Column(name = "hopper_6d_currencycode")
    private Long hopper6DcurrencyCode;

    @Column(name = "hopper_7d_currencycode")
    private Long hopper7dcurrencycode;

    @Column(name = "hopper_8d_currencycode")
    private Long hopper8DcurrencyCode;

    @Column(name = "hopper_1d_billcount")
    private Long hopper1DbillCount;

    @Column(name = "hopper_2d_billcount")
    private Long hopper2DbillCount;

    @Column(name = "hopper_3d_billcount")
    private Long hopper3DbillCount;

    @Column(name = "hopper_4d_billcount")
    private Long hopper4DbillCount;

    @Column(name = "hopper_5d_billcount")
    private Long hopper5DbillCount;

    @Column(name = "hopper_6d_billcount")
    private Long hopper6DbillCount;

    @Column(name = "hopper_7d_billcount")
    private Long hopper7DbillCount;

    @Column(name = "hopper_8d_billcount")
    private Long hopper8DbillCount;

    @Column(name = "hopper_1d_cashrejected")
    private Long hopper1DCashRejected;

    @Column(name = "hopper_2d_cashrejected")
    private Long hopper2DCashRejected;

    @Column(name = "hopper_3d_cashrejected")
    private Long hopper3DCashRejected;

    @Column(name = "hopper_4d_cashrejected")
    private Long hopper4DCashRejected;

    @Column(name = "hopper_5d_cashrejected")
    private Long hopper5DCashRejected;

    @Column(name = "hopper_6d_cashrejected")
    private Long hopper6DCashRejected;

    @Column(name = "hopper_7d_cashrejected")
    private Long hopper7DCashRejected;

    @Column(name = "hopper_8d_cashrejected")
    private Long hopper8DCashRejected;

    @Column(name = "userdefined")
    private String userDefined;

    @Column(name = "hopper_1d_endcash")
    private Long hopper1DEndCash;

    @Column(name = "hopper_2d_endcash")
    private Long hopper2DEndCash;

    @Column(name = "hopper_3d_endcash")
    private Long hopper3DEndCash;

    @Column(name = "hopper_4d_endcash")
    private Long hopper4DEndCash;

    @Column(name = "hopper_5d_endcash")
    private Long hopper5DEndCash;

    @Column(name = "hopper_6d_endcash")
    private Long hopper6DEndCash;

    @Column(name = "hopper_7d_endcash")
    private Long hopper7DEndCash;

    @Column(name = "hopper_8d_endcash")
    private Long hopper8DEndCash;

    @Column(name = "bnaflg")
    private String bnaFlg;

    @Column(name = "bnanotesrefunded")
    private Long bnaNotesRefunded;

    @Column(name = "bnanotesrejected")
    private Long bnaNotesRejected;

    @Column(name = "bnanotesencashed")
    private Long bnaNotesEncashed;

    @Column(name = "bnanotesescrowed")
    private Long bnaNotesEscrowed;

    @Column(name = "activenode_r")
    private String activeNodeR;

}
