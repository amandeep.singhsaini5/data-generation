package com.esq.cashmanagement.data_generation.scheduler;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.esq.cashmanagement.data_generation.entity.Atd;
import com.esq.cashmanagement.data_generation.repository.AtdRepository;

import lombok.extern.log4j.Log4j2;

/**
 * @author Amandeep
 */
@Log4j2
@Component
public class SimulateAtdTransaction {

	@Autowired
	private AtdRepository atdRepository;

	@Value("${addMinutesInAtdTime}")
	private Long addMinutesInAtdTime;

	@Scheduled(fixedRateString = "${simulationInterval}")
	public void simulateTransaction() {
		
		
		
		List<Atd> atdList = atdRepository.findAll();
		
		int countRecords = atdList.size();
		List<Atd> updateAtdList = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			int number = new Random().ints(0, (countRecords)).limit(1).findFirst().getAsInt();
			Atd srcAtd = atdList.get(number);
			Atd result= makeNewTransaction(srcAtd);
			if(result!=null) {
				updateAtdList.add(result);
				log.info("Transaction success.."+srcAtd.getAtmKey());
			}
		}
		atdRepository.saveAll(updateAtdList);

	}

	/**
	 * @author Harvansh.Singh
	 * @param atd
	 * @return updated ATD object 
	 */
	public Atd makeNewTransaction(Atd atd) {
		log.info("Inside makeNewTransaction() AtmKey={} ",atd.getAtmKey());

//		atd.setAtdTime(LocalDateTime.now(ZoneOffset.UTC));
//		atd.setRecordTime(LocalDateTime.now(ZoneOffset.UTC));
//		atd.setLastTransTime(LocalDateTime.now(ZoneOffset.UTC));
//		atd.setLastStatusTime(LocalDateTime.now(ZoneOffset.UTC));
		
		atd.setAtdTime(atd.getAtdTime().plusMinutes(addMinutesInAtdTime));
		atd.setRecordTime(atd.getRecordTime().plusMinutes(addMinutesInAtdTime));
		atd.setLastTransTime(atd.getLastTransTime().plusMinutes(addMinutesInAtdTime));
		atd.setLastStatusTime(atd.getLastStatusTime().plusMinutes(addMinutesInAtdTime));
		
		updateNullValue(atd);
		
		long totalEndCash = atd.getHopper_1_EndCash() + atd.getHopper_2_EndCash() +
				 atd.getHopper_3_EndCash() + atd.getHopper_4_EndCash();
		log.debug("totalEndCash = {}", totalEndCash);

		if (totalEndCash > 150000) {
			
			Set<Integer> selectedHoppers = new HashSet<>();
			
			// select any two hopper from 1-4
			for (int i = 1; i <= 2; i++) {
				int number = new Random().ints(1, (4 + 1)).limit(1).findFirst().getAsInt();
				selectedHoppers.add(number);
			}
			
			// update transaction field for each hoppers and collected total dispensed
			long transactionAmount = selectedHoppers.stream()
					.mapToLong(hoppNum -> updateTransFields(atd, hoppNum))
					.sum();

			if (transactionAmount > 0) {
				log.info("transactionAmount ==" + transactionAmount);
				return atd;
			}
		}
		else {
			doReplenishment(atd);
			return atd;
		}
		return null;
	}

	private void updateNullValue(Atd atd) {
		
		if(atd.getHopper_1_EndCash()==null)
			atd.setHopper_1_EndCash(0l);
		if(atd.getHopper_2_EndCash()==null)
			atd.setHopper_2_EndCash(0l);
		if(atd.getHopper_3_EndCash()==null)
			atd.setHopper_3_EndCash(0l);
		if(atd.getHopper_4_EndCash()==null)
			atd.setHopper_4_EndCash(0l);
		if(atd.getHopper_5_EndCash()==null)
			atd.setHopper_5_EndCash(0l);
		if(atd.getHopper_6_EndCash()==null)
			atd.setHopper_6_EndCash(0l);
		
		//update cashout
		if(atd.getHopper_1_CashOut()==null)
			atd.setHopper_1_CashOut(0l);
		if(atd.getHopper_2_CashOut()==null)
			atd.setHopper_2_CashOut(0l);
		if(atd.getHopper_3_CashOut()==null)
			atd.setHopper_3_CashOut(0l);
		if(atd.getHopper_4_CashOut()==null)
			atd.setHopper_4_CashOut(0l);
		if(atd.getHopper_5_CashOut()==null)
			atd.setHopper_5_CashOut(0l);
		if(atd.getHopper_6_CashOut()==null)
			atd.setHopper_6_CashOut(0l);
		
		//update bill value
		
		if(atd.getHopper_1_billVal()==null)
			atd.setHopper_1_billVal(100l);
		if(atd.getHopper_2_billVal()==null)
			atd.setHopper_2_billVal(200l);
		if(atd.getHopper_3_billVal()==null)
			atd.setHopper_3_billVal(500l);
		if(atd.getHopper_4_billVal()==null)
			atd.setHopper_4_billVal(2000l);
		if(atd.getHopper_5_billVal()==null)
			atd.setHopper_5_billVal(100l);
		if(atd.getHopper_6_billVal()==null)
			atd.setHopper_6_billVal(0l);
		
		
		
		
		
	}

	/**
	 * @author Harvansh.Singh
	 * @param atd
	 * @param hoppNum
	 * @return dispensed amount from given hoppNum
	 */
	public long updateTransFields(Atd atd, int hoppNum) {

		try {
			PropertyDescriptor hopperEndCash = new PropertyDescriptor("hopper_" + hoppNum + "_EndCash", Atd.class);
			java.lang.reflect.Method getterForEC = hopperEndCash.getReadMethod();
			java.lang.reflect.Method setterForEC = hopperEndCash.getWriteMethod();

			PropertyDescriptor hopperbillVal = new PropertyDescriptor("hopper_" + hoppNum + "_billVal", Atd.class);
			java.lang.reflect.Method getterForBV = hopperbillVal.getReadMethod();

			PropertyDescriptor hopperCashOut = new PropertyDescriptor("hopper_" + hoppNum + "_CashOut", Atd.class);
			java.lang.reflect.Method getterForCO = hopperCashOut.getReadMethod();
			java.lang.reflect.Method setterForCO = hopperCashOut.getWriteMethod();

			Long cashDisp = getHopperCashOut((long) getterForBV.invoke(atd), (long) getterForEC.invoke(atd));
			log.debug("in hopper " + hoppNum + " cashDisp={}", cashDisp);
			setterForCO.invoke(atd, (long) getterForCO.invoke(atd) + cashDisp);
			setterForEC.invoke(atd, (long) getterForEC.invoke(atd) - cashDisp);
			
			return cashDisp;

		} catch (IntrospectionException | IllegalAccessException | InvocationTargetException e1) {

			e1.printStackTrace();
		}
		return 0;

	}

	private void doReplenishment(Atd atd) {
		log.debug("Inside doReplenishment() : ");
		Long H1CashIncr = getHopperBegCash(100l, 1000);
		Long H2CashIncr = getHopperBegCash(200l, 600);
		Long H3CashIncr = getHopperBegCash(500l, 300);
		Long H4CashIncr = getHopperBegCash(2000l, 200);

		atd.setHopper_1_begCash(atd.getHopper_1_EndCash() + H1CashIncr);
		atd.setHopper_2_begCash(atd.getHopper_2_EndCash() + H2CashIncr);
		atd.setHopper_3_begCash(atd.getHopper_3_EndCash() + H3CashIncr);
		atd.setHopper_4_begCash(atd.getHopper_4_EndCash() + H4CashIncr);

		atd.setHopper_1_CashIncr(H1CashIncr);
		atd.setHopper_2_CashIncr(H2CashIncr);
		atd.setHopper_3_CashIncr(H3CashIncr);
		atd.setHopper_4_CashIncr(H4CashIncr);

		atd.setHopper_1_CashOut(0l);
		atd.setHopper_2_CashOut(0l);
		atd.setHopper_3_CashOut(0l);
		atd.setHopper_4_CashOut(0l);

		atd.setHopper_1_EndCash(atd.getHopper_1_EndCash() + H1CashIncr);
		atd.setHopper_2_EndCash(atd.getHopper_2_EndCash() + H2CashIncr);
		atd.setHopper_3_EndCash(atd.getHopper_3_EndCash() + H3CashIncr);
		atd.setHopper_4_EndCash(atd.getHopper_4_EndCash() + H4CashIncr);

		log.debug("H1BegCash={} , H2BegCash ={}, H3BegCash ={}, H4BegCash ={}", H1CashIncr, H2CashIncr, H3CashIncr,	H4CashIncr);
	}

	private Long getHopperCashOut(Long billValue, Long endCash) {
		if (billValue != null) {
			Long amount = (long) billValue * new Random().ints(50, (250 + 1)).limit(1).findFirst().getAsInt();
			if (amount <= endCash)
				return amount;
		}
		return 0L;
	}

	private Long getHopperBegCash(Long billValue, int billCount) {
		if (billValue != null) {
			return (long) (new Random().ints(1, (billCount + 1)).limit(1).findFirst().getAsInt() * billValue);
		}
		return 0L;
	}
}
